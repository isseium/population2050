// 引用: view-source:http://www.geosense.co.jp/map/tool/geoconverter.php?cmd=meshcode?cmd=meshcode
// メッシュコードから緯度経度範囲を求める。
// 
//  code  : メッシュコード(ハイフンは入っていても良い）
//
//  戻り値：
//         .error    : 正常終了は0、エラーは負数
//         .lat0     : 左下緯度（度単位）
//         .lon0     : 左下経度
//         .lat1     : 右上緯度
//         .lon1     : 右上経度
//              
// ・この関数では測地系は考慮していない。
function meshCode2LatLon( code )
{
  var num;
  var ret = {};

  code = code.replace( /[^0-9]/g, "" );

  var numDigits = code.length;

  try {
    if ( numDigits < 4 ) throw -1;

    // 一次メッシュ
    var lat = Number( code.substr( 0, 2 ) ) / 1.5;
    var lon = Number( code.substr( 2, 2 ) ) + 100;

    if( lat < -90 || lat > 90 || lon < -180 || lon > 180 ) throw -2;
    if ( numDigits == 4 ) throw 1;
    if ( numDigits == 5 ) throw -3;

    // 二次メッシュ
    var latCode = Number( code.substr( 4, 1 ) );
    var lonCode = Number( code.substr( 5, 1 ) );
    if ( latCode > 7 || lonCode > 7 ) throw -4;

    lat += latCode * 5.0 / 60;
    lon += lonCode * 7.5 / 60;

    if ( numDigits == 6 ) throw 2;

    // 5倍メッシュ
    if ( numDigits == 7 ){
      num = Number( code.substr( 6, 1 ) );
      if ( num < 1 || num > 4 ) throw -5;
      num--;

      latCode = Math.floor( num / 2 );
      lonCode = num - latCode * 2;

      lat += latCode * 5.0 / 2 / 60;
      lon += lonCode * 7.5 / 2 / 60;

      throw 7;
    }

    // 三次メッシュ
    latCode = Number( code.substr( 6, 1 ) );
    lonCode = Number( code.substr( 7, 1 ) );

    lat += latCode * 0.5 / 60;
    lon += lonCode * 0.75 / 60;

    if ( numDigits == 8 ) throw 3;

    // 次のメッシュ
    num = Number( code.substr( 8, 1 ) );
    if ( num < 1 || num > 5 ) throw -6;

    // 2倍メッシュ
    if ( num == 5 ) throw 8;

    // 2分の1メッシュ
    num--;
    latCode = Math.floor( num / 2 );
    lonCode = num - latCode * 2;

    lat += latCode * 0.5 / 2 / 60;
    lon += lonCode * 0.75 / 2 / 60;

    if (numDigits == 9 ) throw 4;

    // 4分の1メッシュ
    num = Number( code.substr( 9, 1 ) );
    if ( num < 1 || num > 4 ) throw -7;

    num--;
    latCode = Math.floor( num / 2 );
    lonCode = num - latCode * 2;

    lat += latCode * 0.5 / 4 / 60;
    lon += lonCode * 0.75 / 4 / 60;

    if (numDigits == 10 ) throw 5;

    // 8分の1メッシュ
    num = Number( code.substr( 10, 1 ) );
    if ( num < 1 || num > 4 ) throw -8;

    num--;
    latCode = Math.floor( num / 2 );
    lonCode = num - latCode * 2;

    lat += latCode * 0.5 / 8 / 60;
    lon += lonCode * 0.75 / 8 / 60;

    throw 6;

  }
  catch( e ){
    if ( e > 0 && e < 9 ) {
      var mlat,mlon;

      if( lat > -90 && lat < 90 && lon > -180 && lon < 180 ){
        ret.error = 0;
        ret.lat0 = lat;
        ret.lon0 = lon;

        switch( e ){
          case 1: mlat = 40; mlon = 60; break;
          case 2: mlat = 5; mlon = 7.5; break;
          case 3: mlat = 0.5; mlon = 0.75; break;
          case 4: mlat = 0.5 / 2; mlon = 0.75 / 2; break;
          case 5: mlat = 0.5 / 4; mlon = 0.75 / 4; break;
          case 6: mlat = 0.5 / 8; mlon = 0.75 / 8; break;
          case 7: mlat = 0.5 * 5; mlon = 0.75 * 5; break;
          case 8: mlat = 0.5 * 2; mlon = 0.75 * 2; break;
        }
        ret.lat1 = ret.lat0 + mlat / 60;
        ret.lon1 = ret.lon0 + mlon / 60;
      }
      else ret.error = -100;
    }
    else {
      ret.error = e;
    }
  }

  return ret;
}

var ymap;
var markers = [];
$(window).resize(function() {
  var csize = $(window).outerHeight(true) - $("#navbar").outerHeight(true);
  $("#map").height(csize);
});
window.onload = function(){
  console.log('Strat');
  $(window).resize();
  var lastZoom = 15;
  var isAccepted = false;
  ymap = new Y.Map("map", {
    configure : {
      doubleClickZoom : true,
      scrollWheelZoom : true,
      singleClickPan : true,
      dragging : true
    }
  });
  ymap.drawMap(new Y.LatLng(39.7035305,141.1526673), 14, Y.LayerSetId.NORMAL);
  ymap.addControl(new Y.LayerSetControl());
  ymap.addControl(new Y.SearchControl());
//  ymap.addControl(new Y.ZoomControl());
  
  var addPoly = function(poly, color, alpha){
    var latlngs = [
      new Y.LatLng(poly.lat0, poly.lon0),
      new Y.LatLng(poly.lat0, poly.lon1),
      new Y.LatLng(poly.lat1, poly.lon1),
      new Y.LatLng(poly.lat1, poly.lon0)
    ];
    
    ymap.addFeature(new Y.Polygon(latlngs, {
      strokeStyle: new Y.Style(color, 4, alpha),
      fillStyle: new Y.Style(color, null, alpha)
    }));
  };
  
  console.log(ymap);
  console.log(ymap.bounds.ne.Lat);
  console.log(ymap.bounds.sw.Lat);
  console.log(ymap.bounds.ne.Lon);
  console.log(ymap.bounds.sw.Lon);

  var makeMesh = function(population){
    var latlon = meshCode2LatLon(population[0] + "");
    var color = "0000ff";
    var alpha = 0.95;
    
    
    diff_lat = ymap.bounds.ne.Lat - ymap.bounds.sw.Lat;
    diff_lon = ymap.bounds.ne.Lon - ymap.bounds.sw.Lon;
    if(latlon.lat0 < ymap.bounds.sw.Lat - diff_lat || latlon.lat1 > ymap.bounds.ne.Lat + diff_lat){
      return;
    } 
    if(latlon.lon0 < ymap.bounds.sw.Lon - diff_lon || latlon.lon1 > ymap.bounds.ne.Lon + diff_lon){
      return;
    } 

    if(population.length === 5){
      var ratio = population[4];

      if(ratio > 100){
        color = "ff0000";
        alpha = ratio / 200.0 ;
        addPoly(latlon, color, alpha);
      }else if(ratio === 0){
        addPoly(latlon, color, alpha);
      }else if(ratio <= 50){
        alpha = (100 - ratio) / 200.0 ;
        addPoly(latlon, color, alpha);
      }else{
        // color = "ffff00";
        // alpha = (150 - ratio) / 200.0 ;
        // addPoly(latlon, color, alpha);
      }

    }
  };

  // 倍率変更時のリスナー
  var reflesh_map = function(){
    console.log('refresh');
    // 初期化
    ymap.clearFeatures();
    
    // ポリゴン
    for(var i=0; i<population2050.length; i++){
      makeMesh(population2050[i]);
    }
  };
  ymap.bind('zoomstart', function(e){
    // 広域のときは警告
    console.log('zoomstart');
    if(e < 12 && isAccepted === false){
      if(!window.confirm('広域表示は処理に時間がかかる場合があります。広域表示しますか？')){
        ymap.setZoom(e + 1);
        return false;
      }
      
      isAccepted = true;
    }
  });
  ymap.bind('zoomend', function(){
    setTimeout(reflesh_map, 100);
  });
  ymap.bind('moveend', function(){
    setTimeout(reflesh_map, 100);
  });
  
  console.log('done');
  
};


// メッシュコードの取得
// 
//  lat,lon  : 緯度経度（度単位）
//
//  戻り値：メッシュコードの配列
//         meshcode[0] : 0=正常終了 負数=エラー
//                 [1] : １次メッシュコード（４桁）
//                 [2] : ２次メッシュコード（２桁）
//                 [3] : ３次メッシュコード（２桁）
//                 [4] : ２分の１メッシュコード（１桁）
//                 [5] : ４分の１メッシュコード（１桁）
//                 [6] : ８分の１メッシュコード（１桁）
//                 [7] : ５倍メッシュコード（１桁）
//                 [8] : ２倍メッシュコード（２桁）
//              
function getMeshCode( lat, lon )
{
  var ret = [];
  var retCode = 0;
  var MINDEG = 0.00000000001;

  try {
    if ( lat < 0 || lon < 0 ) throw -1;

    // 一次メッシュ(緯度４０分、経度１度のメッシュ）
    var lat15m = Math.floor( lat * 1.5);
    var mesh1 = lat15m * 100 + Math.floor(lon) - 100;

    var latResidue = lat - lat15m / 1.5;
    var lonResidue = lon - Math.floor(lon);
    if ( -MINIMUM < latResidue && latResidue < MINIMUM ) latResidue = 0;
    if ( -MINIMUM < lonResidue && lonResidue < MINIMUM ) lonResidue = 0;

    // 二次メッシュ(緯度５分、経度7.5分のメッシュ）
    var lat2 = Math.floor( latResidue / 5.0 * 60  + MINDEG);
    if ( lat2 > 7 ) throw -2;
    var lon2 = Math.floor( lonResidue / 7.5 * 60  + MINDEG);
    if ( lon2 > 7 ) throw -3;

    var mesh2 = lat2 * 10 + lon2;
    if ( mesh2 < 10 ) mesh2 = format0( mesh2 );

    latResidue -= lat2 * 5.0 / 60;
    lonResidue -= lon2 * 7.5 / 60;
    if ( -MINIMUM < latResidue && latResidue < MINIMUM ) latResidue = 0;
    if ( -MINIMUM < lonResidue && lonResidue < MINIMUM ) lonResidue = 0;

    // 三次メッシュ(緯度0.5分、経度0.75分のメッシュ）
    var lat3 = Math.floor( latResidue / 0.5 * 60  + MINDEG);
    if ( lat3 > 9 ) throw -4;
    var lon3 = Math.floor( lonResidue / 0.75 * 60  + MINDEG);
    if ( lon3 > 9 ) throw -5;

    var mesh3 = lat3 * 10 + lon3;
    if ( mesh3 < 10 ) mesh3 = format0( mesh3 );

    latResidue -= lat3 * 0.5 / 60;
    lonResidue -= lon3 * 0.75 / 60;
    if ( -MINIMUM < latResidue && latResidue < MINIMUM ) latResidue = 0;
    if ( -MINIMUM < lonResidue && lonResidue < MINIMUM ) lonResidue = 0;

    // 5倍メッシュ(三次メッシュの５倍メッシュ）
    var mesh5X = Math.floor(lat3 / 5 + MINDEG) * 2 + Math.floor(lon3 / 5 + MINDEG) + 1;

    // 2倍メッシュ(三次メッシュの2倍メッシュ）
    var mesh2X = Math.floor(lat3 / 2 + MINDEG) * 20 + Math.floor(lon3 / 2 + MINDEG) *2;
    if ( mesh2X < 10 ) mesh2X = format0( mesh2X );

    // 2分の1メッシュ(三次メッシュの２分の１メッシュ）
    var lat4 = Math.floor( latResidue / (0.5/2) * 60  + MINDEG);
    if ( lat4 > 1 ) throw -6;
    var lon4 = Math.floor( lonResidue / (0.75/2) * 60  + MINDEG);
    if ( lon4 > 1 ) throw -7;

    var mesh4 = lat4 * 2 + lon4 + 1;

    latResidue -= lat4 * (0.5/2) / 60;
    lonResidue -= lon4 * (0.75/2) / 60;
    if ( -MINIMUM < latResidue && latResidue < MINIMUM ) latResidue = 0;
    if ( -MINIMUM < lonResidue && lonResidue < MINIMUM ) lonResidue = 0;

    // 4分の1メッシュ(三次メッシュの4分の１メッシュ）
    var lat5 = Math.floor( latResidue / (0.5/4) * 60  + MINDEG);
    if ( lat5 > 1 ) throw -8;
    var lon5 = Math.floor( lonResidue / (0.75/4) * 60  + MINDEG);
    if ( lon5 > 1 ) throw -9;

    var mesh5 = lat5 * 2 + lon5 + 1;

    latResidue -= lat5 * (0.5/4) / 60;
    lonResidue -= lon5 * (0.75/4) / 60;

    if ( -MINIMUM < latResidue && latResidue < MINIMUM ) latResidue = 0;
    if ( -MINIMUM < lonResidue && lonResidue < MINIMUM ) lonResidue = 0;

    // 8分の1メッシュ(三次メッシュの8分の１メッシュ）
    var lat6 = Math.floor( latResidue / (0.5/8) * 60  + MINDEG);
    if ( lat6 > 1 ) throw -10;
    var lon6 = Math.floor( lonResidue / (0.75/8) * 60  + MINDEG);
    if ( lon6 > 1 ) throw -11;

    var mesh6 = lat6 * 2 + lon6 + 1;

    // 戻り値
    ret[1] = mesh1;
    ret[2] = mesh2;
    ret[3] = mesh3;
    ret[4] = mesh4;
    ret[5] = mesh5;
    ret[6] = mesh6;
    ret[7] = mesh5X;
    ret[8] = mesh2X;

  }
  catch( e ){
    retCode= e;
    var errstr = "error:" + retCode + " lat2=" + lat2 + " lon2=" + lon2 +
      " lat3=" + lat3 + " lon3=" + lon3 +" lat4=" + lat4 + " lon4=" + lon4 +
      " lat5=" + lat5 + " lon5=" + lon5 +" lat6=" + lat6 + " lon6=" + lon6 +
      " latR=" + latResidue + " lonR=" + lonResidue;
    alert(errstr);
  }

  ret[0] = retCode;
  return ret;
}
