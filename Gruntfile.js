module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    uglify : {
      options: {
        mangle: true,
        compress: {
          drop_console: true
        }
      },
      build : {
        src : [
          'js/src/population2050.json',
          'js/src/utils.js',
        ],
        dest : 'js/population2050.min.js'
      }
    },
  });
  
  grunt.loadNpmTasks('grunt-contrib-uglify');
  
  grunt.registerTask('default', ['uglify']);
};
